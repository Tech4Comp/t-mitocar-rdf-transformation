# T-Mitocar RDF transformation

This repository contains scripts, mappings and a vocabulary to convert T-MITOCAR knowledge maps and T-MITOCAR Artemis maps to RDF by using tarql.

T-MITOCAR creates relations between words and rates these relations with weights.
T-MITOCAR Artemis creates relations between words and assigns both words an area (clusters the map).

## Setup
Install the following dependencies in order to run the scripts successfully.
* catdoc (providing xls2csv command)
* tarql
* raptor2 (providing rapper command)
* sed

tarql is expected to be installed to the system and to be available on the CLI as `tarql`.

## Usage

Type `./transcode.sh -h` to see usage information. Use the example files to get started.
