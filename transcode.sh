#!/bin/bash

printhelp () {
  echo "Usage: ./transcode.sh -i inputFile -m areas|weights|weights_complex [-c]"
	echo "This script will transcode the provided data to turtle data"
  echo "-i supports xls and csv files"
  echo "-m supports areas for T-Mitocar Artemis output, weights and weights_complex for T-Mitocar output"
  echo "-c will compress the resulting turtle file"
}

if (($# == 0)); then
  echo "No arguments provieded!"
  printhelp
  exit 1;
fi

while getopts hi:m:c options ; do
 case "${options}" in
 i)
    if [ -f "$OPTARG" ] ; then
      inputfile=${OPTARG}
    else
      echo "Specified file does not exist!"
      exit 1 ;
    fi
    ;;
 m) mode=${OPTARG} ;;
 c) compress=true ;;
 h) printhelp; exit ;;
 :)
    echo "Error: -${OPTARG} requires an argument."
    exit 1
    ;;
 *) exit;;
 \?) echo "Unknown option: -$OPTARG" >&2; exit 1 ;;
 esac
done

compress=${compress:-false}

if [ ! "$mode" = "areas" ] && [ ! "$mode" = "weights" ] && [ ! "$mode" = "weights_complex" ]; then
	echo "-m needs to be either areas, weights or weights_complex."
  exit 1 ;
fi

filename=$(basename -- "$inputfile")
extension="${filename##*.}"
filename="${filename%.*}"

if [ ! "$extension" = "xls" ] && [ ! "$extension" = "csv" ]; then
  echo "The input file either need to be a csv or xls file."
  exit 1 ;
fi

if [ "$extension" = "xls" ]; then
	#libreoffice --headless --convert-to csv "$inputfile" --outdir "$PWD/"
	xls2csv -q 0 -d utf-8 "$inputfile" > "${inputfile%.$extension}.csv"
fi

# sed -i -e 's/ü/ue/Ig' -e 's/ä/ae/Ig' -e 's/ö/oe/Ig' -e 's/ß/ss/Ig' "${1%.xls}.csv"

if [ "$mode" = "areas" ]; then
	tarql -e utf-8 --dedup 1000 $PWD/mapping_areas.sparql "${inputfile%.$extension}.csv" | tee "${inputfile%.$extension}.ttl"
elif [ "$mode" = "weights" ]; then
	tarql -e utf-8 --dedup 1000 $PWD/mapping_weights_simple.sparql "${inputfile%.$extension}.csv" | tee "${inputfile%.$extension}.ttl"
elif [ "$mode" = "weights_complex" ]; then
	tarql -e utf-8 --dedup 1000 $PWD/mapping_weights.sparql "${inputfile%.$extension}.csv" | tee "${inputfile%.$extension}.ttl"
fi

#optional step to compress turtle
if [ "$compress" = true ]; then
  rapper -i turtle -o turtle "${inputfile%.$extension}.ttl" > "${inputfile%.$extension}_compressed.ttl"
  echo "wrote results to ${inputfile%.$extension}_compressed.ttl"
else
  echo "wrote results to ${inputfile%.$extension}.ttl"
fi
